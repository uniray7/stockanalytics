import os, sys
import math
from typing import List
from glob import glob
from queue import Queue
import pandas as pd
from bokeh.plotting import figure, output_file, show, ColumnDataSource
from bokeh.models.tools import HoverTool

from utils import convert_to_nday_MA, convert_to_ratio

csv_path_list = sorted(glob('./data/exchange/*.csv'))
x_label = []
close_price_list = []


for csv_path in csv_path_list:
    x_label.append(os.path.split(csv_path)[-1].replace('.csv', ''))

    df = pd.read_csv(csv_path)
    df.set_index('證券代號', inplace=True)
    close_price = float(df.loc['2330']['收盤價'])

    close_price_list.append(close_price)

ma5_list = convert_to_nday_MA(close_price_list, 5)
ma10_list = convert_to_nday_MA(close_price_list, 10)
ma20_list = convert_to_nday_MA(close_price_list, 20)

output_file("2330.html")

# create a new plot
p = figure(
   tools="pan,box_zoom,reset,save",
   y_range=[min(close_price_list), max(close_price_list)], title="log axis example",
   x_axis_label='sections', y_axis_label='particles',
   x_range=x_label, plot_width=1800
)

# add some renderers
p.line(x_label, close_price_list, legend="2330")
p.line(x_label, ma5_list, legend="2330 MA5", line_color="red")
p.line(x_label, ma10_list, legend="2330 MA10", line_color="green")
p.line(x_label, ma20_list, legend="2330 MA20", line_color="black")


p.xaxis.major_label_orientation = math.pi/4
p.add_tools(HoverTool(tooltips=[("close price", "@y"), ("date", "@x")]))
show(p)

close_price_ratio_list = convert_to_ratio(close_price_list)
ma5_ratio_list = convert_to_ratio(ma5_list)
ma10_ratio_list = convert_to_ratio(ma10_list)
ma20_ratio_list = convert_to_ratio(ma20_list)

output_file("2330_ratio.html")
p2 = figure(
   tools="pan,box_zoom,reset,save",
   y_range=[-0.1, 0.1], title="log axis example",
   x_axis_label='sections', y_axis_label='particles',
   x_range=x_label, plot_width=1800
)
p2.xaxis.major_label_orientation = math.pi/4

# add some renderers
p2.line(x_label, close_price_ratio_list, legend="2330 close price ratio")
p2.line(x_label, ma5_ratio_list, legend="2330 MA5 ratio", line_color="red")
p2.line(x_label, ma10_ratio_list, legend="2330 MA10 ratio", line_color="green")
p2.line(x_label, ma20_ratio_list, legend="2330 MA20 price ratio", line_color="black")

p2.add_tools(HoverTool(tooltips=[("close price", "@y"), ("date", "@x")]))

show(p2)