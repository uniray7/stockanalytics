def convert_to_nday_MA(price_list: List[float], nday: int) -> List[float]:
    '''
    price list: the last element of list is the latest price
    '''
    res = [0] * len(price_list)
    for idx in range(len(price_list)):
        if idx >= nday-1:
            res[idx] = sum(price_list[idx-nday+1:idx+1])/ nday
    return res


def convert_to_ratio(val_list: List[float]) -> List[float]:
    last_val = 0
    res = [0] * len(val_list)
    for idx in range(len(val_list)):
        if last_val != 0 and idx > 0:
            res[idx] = (val_list[idx] - last_val) / last_val
        last_val = val_list[idx]
    return res


def extract_col_of_stocks(stock_nums: List[str], cols: List[str]):
    for csv_path in csv_path_list:
        x_label.append(os.path.split(csv_path)[-1].replace('.csv', ''))

        df = pd.read_csv(csv_path)
        df.set_index('證券代號', inplace=True)
        close_price = float(df.loc['2330']['收盤價'])

        close_price_list.append(close_price)
